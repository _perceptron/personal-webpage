Map {
  background-color: #f5f5f5;
}

#countries {
  ::outline {
    line-color: #555555;
    line-width: 2;
    line-join: round;
  }
  polygon-fill: #aaaaaa;
}

#visited_places {
   marker-width:10;
   marker-fill:#f6b437;
   marker-line-color:#000000;
   marker-allow-overlap:true;

}